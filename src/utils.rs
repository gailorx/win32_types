pub struct WideString {
    string: Vec<u16>,
}
impl WideString {
    pub fn from_str(string: &str) -> WideString {
        WideString {
            string: string.utf16_units().chain(Some(0).into_iter()).collect()
        }
    }
    pub fn as_ptr(&self) -> *const u16 {
        self.string.as_ptr()
    }
}
